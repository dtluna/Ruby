require "gtk3"

builder = Gtk::Builder.new
builder.add_from_file('ui.glade')
window = builder.get_object('window')
window.signal_connect('destroy') {
  Gtk::main_quit
}
window.show_all

Gtk::main
